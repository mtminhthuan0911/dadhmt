import pgzrun

WIDTH = 600
HEIGHT = 500 
vx = 5
vy = 5

game_over = False
start_game = True

ball = Rect((550 , 500), (100,100))
bat1 = Rect((580,200), (20,80))
bat2 = Rect((0,250), (20,80))
backgroundBegin=Actor('begin3')
score_bat1 = 0
score_bat2 = 0
ongs = []

def draw():
    screen.clear()
    if start_game:
        backgroundBegin.draw()
    else:
        if game_over:
            screen.draw.line((WIDTH/2,0),(WIDTH/2,HEIGHT), "black")
        else:
            screen.draw.line((WIDTH/2,0),(WIDTH/2,HEIGHT), "white")
        screen.draw.filled_rect(ball, "yellow")
        screen.draw.filled_rect(bat1, "blue")
        screen.draw.filled_rect(bat2, "green")
        screen.draw.text('Player A = {}'.format(score_bat1) ,(50,100/2), color="green" , fontsize=50)
        screen.draw.text('Player B = {}'.format(score_bat2) ,(350,100/2), color="blue" , fontsize=50)
        if game_over and ball.x>WIDTH:
            screen.draw.text('YOU WIN', (50,HEIGHT/2), fontsize=50)
            screen.draw.text('YOU LOSE', (350,HEIGHT/2), fontsize=50)
            screen.draw.text('PRESS SPACE TO RESTART', (70,300), color="red" , fontsize=50)
        elif game_over and ball.x<0:
            screen.draw.text('YOU LOSE', (50,HEIGHT/2), fontsize=50)
            screen.draw.text('YOU WIN', (350,HEIGHT/2), fontsize=50)
            screen.draw.text('PRESS SPACE TO RESTART', (70,300), color="red" , fontsize=50)
def update():
    global vx,vy,game_over,ball,bat1,bat2,start_game,score_bat1,score_bat2
    if start_game:
        ball.x+=0
        ball.y+=0
    else:
        ball.x+=vx
        ball.y+=vy
    if game_over:
        ball.x+=0
        ball.y+=0
    if ball.top < 0 or ball.bottom > HEIGHT:
        vy=-vy
    if ball.colliderect(bat1) or ball.colliderect(bat2):
        vx=-vx
    if keyboard.up:
        bat1.y-=4
    if keyboard.down:
        bat1.y+=4
    if keyboard.w:
        bat2.y-=4
    if keyboard.s:
        bat2.y+=4
    if ball.x<0 or ball.x>WIDTH:
        game_over = True
    if ball.x==0:
        score_bat2 = score_bat2 + 1
    if ball.x==WIDTH:
        score_bat1 = score_bat1 + 1
    if keyboard.f:
        start_game = False
        game_over = False
        ball = Rect((550 , 200), (20,20))
        bat1 = Rect((580,200), (20,80))
        bat2 = Rect((0,250), (20,80))
    if keyboard.space:
        game_over = True
        ball = Rect((550 , 200), (20,20))
        bat1 = Rect((580,200), (20,80))
        bat2 = Rect((0,250), (20,80))
pgzrun.go()
